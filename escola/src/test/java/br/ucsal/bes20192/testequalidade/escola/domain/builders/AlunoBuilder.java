package br.ucsal.bes20192.testequalidade.escola.domain.builders;

import br.ucsal.bes20192.testequalidade.escola.domain.Aluno;
import br.ucsal.bes20192.testequalidade.escola.domain.SituacaoAluno;

public class AlunoBuilder {

    public static final String DEFAULT_NOME = "Fulano da Silva";
    public static final SituacaoAluno DEFAULT_SITUACAO = SituacaoAluno.ATIVO;
    public static final Integer DEFAULT_ANO_NASCIMENTO = 1990;

    private Integer matricula;
    private String nome = DEFAULT_NOME;
    private SituacaoAluno situacao = DEFAULT_SITUACAO;
    private Integer anoNascimento = DEFAULT_ANO_NASCIMENTO;

    private AlunoBuilder(){}

    /**
     * Inicia uma construção
     * @return AlunoBuilder
     */
    public static AlunoBuilder aluno() {
        return new AlunoBuilder();
    }

    /**
     * Inicia uma contrução com valores default
     * @return AlunoBuilder
     */
    public static AlunoBuilder alunoDefault() {
        return AlunoBuilder.aluno()
            .withNome(DEFAULT_NOME)
            .withAnoNascimento(DEFAULT_ANO_NASCIMENTO)
            .withSituacao(DEFAULT_SITUACAO);
    }

    /**
     * Inicia uma construção com valores default e situação ATIVO
     * @return AlunoBuilder
     */
    public static AlunoBuilder alunoAtivo() {
        return AlunoBuilder.alunoDefault()
            .asAtivo();
    }

    /**
     * Inicia uma construção com valores default e situação CANCELADO
     * @return AlunoBuilder
     */
    public static AlunoBuilder alunoCancelado() {
        return AlunoBuilder.alunoDefault()
            .asCancelado();
    }

    /**
     * Adiciona a matrícula
     * @param matricula
     * @return AlunoBuilder
     */
    public AlunoBuilder withMatricula(Integer matricula) {
        this.matricula = matricula;
        return this;
    }

    /**
     * Adiciona o nome
     * @param nome
     * @return AlunoBuilder
     */
    public AlunoBuilder withNome(String nome) {
        this.nome = nome;
        return this;
    }

    /**
     * Aidiciona o ano de nascimento
     * @param anoNascimento
     * @return AlunoBuilder
     */
    public AlunoBuilder withAnoNascimento(Integer anoNascimento) {
        this.anoNascimento = anoNascimento;
        return this;
    }

    /**
     * Define situação como ATIVO
     * @return AlunoBuilder
     */
    public AlunoBuilder asAtivo() {
        this.situacao = SituacaoAluno.ATIVO;
        return this;
    }

    /**
     * Define situação como CANCELADO
     * @return AlunoBuilder
     */
    public AlunoBuilder asCancelado() {
        this.situacao = SituacaoAluno.CANCELADO;
        return this;
    }

    /**
     * Adiciona uma situação
     * @param situacao
     * @return AlunoBuilder
     */
    public AlunoBuilder withSituacao(SituacaoAluno situacao) {
        this.situacao = situacao;
        return this;
    }

    /**
     * Replica o construtor em uma nova instância
     * @return AlunoBuilder
     */
    public AlunoBuilder but(){
        return AlunoBuilder.aluno()
            .withNome(this.nome)
            .withAnoNascimento(this.anoNascimento)
            .withMatricula(this.matricula)
            .withSituacao(this.situacao);
    }

    /**
     * Finaliza a construção do objeto
     * @return Aluno
     */
    public Aluno build() {
        Aluno instance = new Aluno();
        instance.setNome(this.nome);
        instance.setMatricula(this.matricula);
        instance.setAnoNascimento(this.anoNascimento);
        instance.setSituacao(this.situacao);
        return instance;
    }
}
