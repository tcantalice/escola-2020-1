package br.ucsal.bes20192.testequalidade.escola.business;


import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import br.ucsal.bes20192.testequalidade.escola.domain.Aluno;
import br.ucsal.bes20192.testequalidade.escola.domain.builders.AlunoBuilder;
import br.ucsal.bes20192.testequalidade.escola.persistence.AlunoDAO;
import br.ucsal.bes20192.testequalidade.escola.util.DateHelper;


public class AlunoBOIntegradoTest {

	public static AlunoDAO dao;
	public static AlunoBuilder builder;
	public static AlunoBO bo;

	@BeforeAll
	public static void setup() {
		dao = new AlunoDAO();
		builder = AlunoBuilder.alunoDefault();
		bo = new AlunoBO(dao,new DateHelper());
	}

	@BeforeEach
	public void reset() {
		dao.excluirTodos();
	}

	/**
	 * Verificar o calculo da idade. Caso de teste: aluno nascido em 2003 tera 16
	 * anos. Caso de teste 
	 * # | entrada 				 | saida esperada 
	 * 1 | aluno nascido em 2003 |	 * 17
	 */
	@Test
	public void testarCalculoIdadeAluno1() {
		Integer matricula = 1234;

		Aluno aluno = builder.but()
			.withAnoNascimento(2003)
			.withMatricula(matricula)
			.build();

		dao.salvar(aluno);

		Integer idadeEsperada = 17;
	    assertEquals(idadeEsperada, bo.calcularIdade(matricula));
	}
	

	/**
	 * Verificar se alunos ativos sao atualizados.
	 */
	@Test
	public void testarAtualizacaoAlunosAtivos() {
		Integer matricula = 1234;

		Aluno aluno = builder.but()
			.asAtivo()
			.withMatricula(matricula)
			.build();

		bo.atualizar(aluno);
		assertEquals(aluno, dao.encontrarPorMatricula(matricula));
	}

}
